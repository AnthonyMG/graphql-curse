import mongoose from 'mongoose'

mongoose.Promise = global.Promise
mongoose.set('useFindAndModify', false);
mongoose.connect('mongodb://localhost/tienda', {useNewUrlParser: true})

const clientesSchema = mongoose.Schema({
    nombre : String,
    apellido : String,
    empresa : String,
    emails : Array,
    edad : Number,
    tipo : String,
    pedido : Array

})
const productoSchema =  mongoose.Schema({
    nombre  : String,
    precio : Number,
    stock : Number
})

const Productos = mongoose.model('productos', productoSchema)
const Clientes = mongoose.model('clientes', clientesSchema)

export {Clientes , Productos}