import mongoose from 'mongoose'
import {Clientes , Productos } from './db'

export const resolvers = {
    Query : {
        getClientes :(root,{limite , offset})=>{
            return Clientes.find({}).limit(limite).skip(offset)
        } ,
        getCliente : (root,{id})=> {
            return new Promise((resolve,object)=>{
                Clientes.findById(id,(error,cliente)=>{
                    if(error) rejects(error)
                    else resolve(cliente)
                })
            })
        },
        totalClientes : ()=>{
            return new Promise((resolve,object)=>{
                Clientes.countDocuments({},(error,count)=>{
                    if(error) rejects(error)
                    else resolve(count)
                })
            })
        },
        
        obtenerProductos : (root , {limite , offset})=>{
            return Productos.find({}).limit(limite).skip(offset) 
        },
        obtenerProducto : (root , {id})=>{
            return new Promise((resolve,reject) => {
                Productos.findById(id,(error, producto)=>{
                    if(error) reject(error)
                    else resolve(producto)
                })
            })
        } , 
        hola : ({id}) => "hola " + id 
    },
    Mutation : {
        crearCliente : (root, {input})=>{
            console.log(input.emails)
            const nuevoCliente =  new Clientes({
               nombre :input.nombre,
               apellido :input.apellido,
               empresa :input.empresa,
               emails :input.emails,
               edad :input.edad,
               tipo :input.tipo,
               pedido :input.pedido
            })
            return new Promise((resolve, reject)=>{
                nuevoCliente.save((error)=>{
                    if(error) rejects(error)
                    else resolve(nuevoCliente)
                })
            })

        },
        actualizarCliente : (root,{input})=>{
           return new Promise((resolve , object)=>{
                Clientes.findOneAndUpdate({_id: input.id} , input , {new : true} , (error,cliente)=>{
                     if(error) rejects(error)
                     else resolve(cliente)   
                })
           })
        },
        eliminarCliente : ( root, {id}) =>{
            return new Promise((resolve , object)=>{
                Clientes.findOneAndRemove({_id: id}, (error)=>{
                     if(error) rejects(error)
                     else resolve("Se elimino correctamente")   
                })
           })
        },
        nuevoProducto : (root , args) =>{
            console.log(args)
            const {nombre , precio , stock } = args.input
            
            const nuevoProducto = new Productos({
                nombre,
                precio,
                stock
            })
            nuevoProducto.id = nuevoProducto._id
            return  new Promise((resolve, rejects)=>{
                nuevoProducto.save((error)=>{
                    if(error)  rejects(error)
                    else resolve(nuevoProducto)
                })
            })
        },
        actualizarProducto : (root , {input})=>{
            return new Promise((resolve, object)=>{
                const {id} = input
                delete input.id
                Productos.findOneAndUpdate({_id : id} , {...input}  , {new : true} , (error , producto)=>{
                    if(error) rejects(error)
                    else resolve(producto)
                })
            })
        },
        eliminarProducto : (root , { id }) =>{
            return new Promise((resolve,object)=>{
                Productos.findOneAndRemove({_id:id} ,(error)=>{
                    if(error) rejects(error)
                    else resolve("Se elimino correctamente")
                })
            })
        }
    }

}

